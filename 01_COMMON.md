## Common

-   Qu'est ce que l'"`UTF-8`" ?
-   Que veut dire "`UTC`" ?
-   Quel moyen peut-on utiliser pour faire communiquer un programme Java et un autre en C ?
-   Quel sont les avantages / inconvénients / cas d'usages des langages suivants (en quelques mots):
    -   unix shell
    -   Java
    -   Php
    -   Python
    -   COBOL
-   Citez au moins 2 gestionnaires de versions de code.
-   Citez au moins 2 distributions linux.
-   Pourquoi le logo de Java est-il une tasse de café ?
-   Quel est votre IDE préféré ? Éditeur de texte préféré ?
-   À quoi sert un "`git rebase`" ?
-   Citez quelques sites de veille technologique.
-   Qu'est ce qu'une "pull request" ou "merge request" ?
-   Donnez un exemple de données concernant une personne :
    -   en XML
    -   en JSON
    -   en Yaml
    -   en CSV
-   Citez quelques exemples de frameworks de tests unitaires (Java, Php, javascript, ...)
-   Citez quelques exemples d'outils de contrôle de qualité de code.
-   Écrivez 42 en binaire et en hexadécimal.
-   Qu'est ce que l'intégration continue ?
-   Le code, en français ou en anglais ?

* * *
