## Web

-   Citez 2 frameworks javascript
-   Qu'apporte HTML5 sur ses prédécesseurs ?
-   Avantages / inconvénients d'utiliser un CDN
-   Qu'est ce qu'un "load balancer" ?
-   Qu'est ce qui passe en général par le port 22 ?
-   Qu'est ce qui passe en général par le port 443 ?
-   Quelle couleur correspond au code `#19AE1B` ?
-   Citez 3 serveurs web / applicatifs.
-   À quoi sert un proxy ?
-   À quoi correspond le code `HTTP 403` ?
-   Quelles méthodes standard doit exposer une API RESTful ?

* * *
