#!/bin/bash

# will run only in a docker env
[[ ! -e /.dockerenv ]] && exit 0

# Display debug in console
if [ -n "${DEBUG}" ]; then
    set -xe
fi

BUILD_FOLDER="technical-testing"
mkdir -p "${BUILD_FOLDER}"

# Concat all markdown documents
cat ./??_*.md > ${BUILD_FOLDER}/TECHNICAL_TESTING.md

# Build formatted documents
pandoc --from=markdown --to=odt     --output=${BUILD_FOLDER}/TECHNICAL_TESTING.odt ${BUILD_FOLDER}/TECHNICAL_TESTING.md
pandoc --from=markdown --to=latex   --output=${BUILD_FOLDER}/TECHNICAL_TESTING.pdf ${BUILD_FOLDER}/TECHNICAL_TESTING.md
