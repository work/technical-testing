#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

# Display debug in console
if [ -n "${DEBUG}" ]; then
    set -xe
fi

DEBIAN_FRONTEND=noninteractive
TZ=Europe/Paris
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install missing dependencies
apt-get update -yqq
apt-get install -yqq pandoc texlive-latex-recommended texlive-latex-extra
