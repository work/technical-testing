## Php (Symfony)

-   À quoi sert Doctrine ?
-   Quelles sont les différences majeures entre la version 4 et la version 3 ?
-   Quel dossier standard d'une application Symfony doit être servi par le serveur web ?
-   Sur quel outil de gestion de dépendances le framework Symfony est-il basé ?

* * *
