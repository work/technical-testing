## Security

-   Qu'est ce qu'une faille XSS ? Comment s'en prémunir ?
-   Qu'est ce qu'une faille CSRF ? Comment s'en prémunir ?
-   Qu'est ce qu'une injection SQL ? Comment s'en prémunir ?
-   Qu'est ce qu'un "buffer overflow" ? Comment s'en prémunir ?
-   Qu'est ce qu'un bon mot de passe ?
-   À quoi sert un "salt" ?
-   `md5` est il un moyen fiable de hasher un mot de passe ?
-   Comment être certain que l'utilisateur ne modifie pas le code javascript exécuté sur son navigateur ?

* * *
