## Algorithms

_Sauf mention contraire, le choix du langage est laissé au candidat (pseudo-code accepté)._

-   Écrire une fonction qui inverse une chaîne de caractères.
-   Écrire une fonction qui, pour chaque entier de 1 à 100, affiche le nombre suivi de `foo` si celui-ci est multiple de 3, le nombre suivi de `bar` si celui-ci est multiple de 7 et le nombre suivi de `foobar` si celui-ci est multiple de 3 et de 7.
-   Écrire une fonction `contains(tree, n)` qui renvoie un booléen si l'arbre binaire `tree` ou une de ses branches a une propriété `value` valant `n`. L'arbre et chacune de ses branches a une propriété `value` (numérique) et peut avoir une propriété `left` et/ou une propriété `right` contenant également un arbre ou branche du même type.

* * *
