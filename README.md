# Technical testing

Some questions that should be asked to a candidate during recruitment.

## Categories

-   [Common](01_COMMON.md)
-   [Databases](02_DATABASES.md)
-   [Web](03_WEB.md)
-   [Security](04_SECURITY.md)
-   [Php](05_PHP.md)
-   [Php (Symfony)](06_PHP-SYMFONY.md)
-   [Java](07_JAVA.md)

* * *
